#!/usr/bin/env bash
# FIXME: We're stuck here because of the emulate-fhs bug...
set -euxo pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"/"
# FIXME Below is an alteration to PATH and other calisthenics made necessary by
# the fact that Guix's version of texlive basically does not work.
#
# The first consequence is that one has to install texlive some other way, and
# refer to it down here
if [ -z "${TEXLIVE_DIR:-}" ]
then
    TEXLIVE_DIR="/usr/bin/texlive"
fi
PATH="$TEXLIVE_DIR/bin/$(uname -m)-linux/:$PATH"
# The second consequence is that we must run in a guix container because texlive
# expects things to be at some places, and if they are missing it does not work (e.g. bash).
# We therefore need to use the --emulate-fhs option, but it only works withing a container
if [ -z "${GUIX_ENVIRONMENT:-}" ]
then
    guix shell --container \
        --share="$TEXLIVE_DIR" --share="$HOME/.cache/common-lisp/" \
        --expose="$SCRIPT_DIR" \
        --emulate-fhs --preserve=TEXLIVE_DIR --file="$SCRIPT_DIR/guix.scm" -- \
        $0 "$@"
    exit 0
fi
# End of FIXME
ASDF_OUTPUT_TRANSLATIONS="/:$HOME/.cache/common-lisp/my-dir/" \
    rlwrap sbcl \
    --load $GUIX_ENVIRONMENT/share/common-lisp/source/asdf/asdf.lisp \
    --eval  '(setf asdf:*central-registry* '"'"'(#P"'$SCRIPT_DIR'"))' \
    --load "${SCRIPT_DIR}/packslide.lisp" \
    --load "$1"

;; FIXME: Clean this up with asdf
(asdf:load-system :cl-smt-lib)
(asdf:load-system :opticl)
(asdf:load-system :cl-grnm)
(asdf:load-system :djula)
(in-package :cl-smt-lib)
(in-readtable :cl-smt-lib)

;; See https://github.com/GrammaTech/cl-smt-lib/issues/5 : the empty list in
;; declare-fun is written as NIL to the smt solver
(defmethod print-object :around ((object (eql nil)) stream)
      (write-string "()" stream))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Classes and shortcuts to instanciate them ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass item ()
  ((name :accessor name
         :initarg :name
         :documentation "A unique way to refer to this item instance specifically")
   (height :accessor height
           :documentation "Vertical size")
   (width :accessor width
          :documentation "Horizontal size")
   (x :accessor x
      :documentation "Horizontal position, 0 is leftmost")
   (y :accessor y
      :documentation "Vertical position, 0 is lowermost"))
  (:documentation "A part of a slide, assumed to be rectangular, to be optimally
  packed in to a slide, minimizing the amount of unused space."))

(defclass multiline (text)
  ()
  (:documentation "A mixin to compute the possible sizes of an item whose
  textual content may be broken between multitple lines"))

(defclass single-line (text)
  ()
  (:documentation "A mixin to compute the possible sizes of an item whose
  textual content must stay in one line."))

(defclass text ()
  ((text :accessor text
         :initarg :text
         :documentation "The content"))
  (:documentation "A mixin for all items that contain text"))

(defclass title (item)
  ()
  (:documentation "A per-slide unique bigger piece of text.
It will be placed left of or above all other items."))
(defparameter *title-position* :top
  ":top means the title is the topmost element of a slide,
:bottom means the title is bottommost element of a slide,
:top-origin means the title starts higher than any other element,
:bottom-origin means the title ends lower than any other element
any other value means there is no constraint on the vertical position of the title on the slide.")

(defclass multiline-title (title multiline)
  ()
  (:documentation "A title so long, it can be broken over multiple lines"))

(defclass single-line-title (title single-line)
  ()
  (:documentation "The default title"))

(defclass paragraph (item multiline)
  ()
  (:documentation "A multiline block of text."))

(defclass bullet (item single-line)
  ()
  (:documentation "A single line block of text with a bullet in front"))

(defclass url (item single-line)
  ()
  (:documentation "A single line of text, formated as a hyperlink"))

(defclass image (item)
  ((path :accessor path
         :initarg :path
         :documentation "The path to the image file")
   (min-size :accessor min-size
             :initarg :min-size
             :initform 0
             :documentation "The value below which alpha will not go when computing the size")
   (pixel-height :accessor pixel-height
                :initarg :pixel-height
                :documentation "The vertical size in pixels")
   (pixel-width :accessor pixel-width
                :initarg :pixel-width
                :documentation "The horizontal size in pixels"))
  (:documentation "An image from a file"))

(defparameter *nb-items* 0
  "Part of a slide context: the number of items so far.")

(defun image (path &key (min-size 0))
  "Return an image with its path slot set to PATH, and a unique name generated from the context"
  (incf *nb-items*)
  (destructuring-bind (pixel-height pixel-width _)
      (array-dimensions (opticl:read-image-file path))
    (declare (ignore _))
    (make-instance 'image :path path :name (format nil "image-~A" (1- *nb-items*))
                          :pixel-width pixel-width :pixel-height pixel-height
                          :min-size min-size)))

(defun title (text &key (multiline nil))
  "Return a title with its textual contents set to TEXT, and a unique name
generated from the context.

If multiline is set to T, the title contents may be broken up in multiple
lines."
  (incf *nb-items*)
  (make-instance (if multiline 'multiline-title 'single-line-title)
                 :text text :name (format nil "title-~A" (1- *nb-items*))))

(defun bullet (text)
  "Return a bullet with its textual contents set to TEXT, and a unique name
generated from the context."
  (incf *nb-items*)
  (make-instance 'bullet
                 :text text
                 :name (format nil "bullet-~A" (1- *nb-items*))))


(defun url (text)
  "Return a url with its textual contents set to TEXT, and a unique name
generated from the context."
  (incf *nb-items*)
  (make-instance 'url
                 :text text
                 :name (format nil "bullet-~A" (1- *nb-items*))))

(defun p (text)
  "Return a paragraph with its textual contents set to TEXT, and a unique name
generated from the context."
  (incf *nb-items*)
  (make-instance 'paragraph
                 :text text
                 :name (format nil "paragraph-~A" (1- *nb-items*))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generate the smt lib rules ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defgeneric smt-single-rules (i)
  (:documentation "Return all the smt-lib forms for item i")
  (:method-combination append))

(defvar *position-suffixes* '("-x" "-y" "-width" "-height")
  "For an item called toto, the four positioning smt variables toto-x, toto-y,
toto-width, and toto-height fully define the position and size of the item on
the slide.")

(defun symcat (name suffix)
  "Return the symbol resulting from the concatenation of name and suffix"
  (intern (concatenate 'string name suffix)))

(defun smt-position-variables (name)
  "Return the position variables for name"
  (loop for suffix in *position-suffixes*
        collect (symcat name suffix)))

(defun smt-var (name)
  "Return the smt-lib forms that define the positioning variables for an item
named NAME."
  (loop for var in (smt-position-variables name)
        ;; Declare the variable
        collect #!`(declare-fun ,VAR () Int)
        ;; Make it positive
        collect #!`(assert (>= ,VAR 0))))

(defun smt-fit-page (name)
  "Return the smt-lib forms that make sure that the item named NAME is within
the slide boundaries."
  #!`((assert (<= (+ ,(SYMCAT NAME "-x") ,(SYMCAT NAME "-width")) page-width))
      (assert (<= (+ ,(SYMCAT NAME "-y") ,(SYMCAT NAME "-height")) page-height))))

(defmethod smt-single-rules append ((i item))
  "Return the smt-lib forms for item i"
  (append
   (smt-var (name i))
   (smt-fit-page (name i))))

(defgeneric smt-pair-rules (first second)
  (:documentation "Return all the smt-lib forms for the given pair of item")
  (:method-combination append))

(defmethod smt-pair-rules append ((first title) (second text))
  "Return the smt-lib forms that will make the title bigger than any other text"
  #!`((assert (> ,(SYMCAT (NAME FIRST) "-height") ,(SYMCAT (NAME SECOND) "-height")))))

(defmethod smt-pair-rules append ((first title) (second item))
  "Return the smt-lib forms for the title position w.r.t. all the other elements"
  (case *title-position*
    (:top ;; Make the title be above everything else
     (let ((nfirst (name first))
           (nsecond (name second)))
       #!`((assert ,(SMT-BELOW NSECOND NFIRST)))))
    (:bottom ;; Make the title be below everything else
     (let ((nfirst (name first))
           (nsecond (name second)))
       #!`((assert ,(SMT-BELOW NFIRST NSECOND)))))
    (:top-origin ;; The title starts higher than anything else, but there may be elements sharing some of its rows
     #!`((assert (> ,(SYMCAT (NAME FIRST) "-y") ,(SYMCAT (NAME SECOND) "-y")))))
    (:bottom-origin ;; The title ends lower than anything else, but there may be elements sharing some of its rows
     #!`((assert (< (+ ,(SYMCAT (NAME FIRST) "-y") ,(SYMCAT (NAME FIRST) "-height")) ,(SYMCAT (NAME SECOND) "-y")))))
    (otherwise '())))

(defmethod smt-pair-rules append ((first item) (second title))
  "Return the smt-lib forms that will place the title on the left or above all other items"
  ;; Only implemented once, see above
   (smt-pair-rules second first))

(defun smt-left (first second)
  "Return the smt-lib form that will place the item named FIRST on the left of
the item named SECOND"
  #!`(<= (+ ,(SYMCAT FIRST "-x") ,(SYMCAT FIRST "-width")) ,(SYMCAT SECOND "-x")))

(defun smt-below (first second)
  "Return the smt-lib form that will place the item named FIRST below the item
named SECOND"
  #!`(<= (+ ,(SYMCAT FIRST "-y") ,(SYMCAT FIRST "-height")) ,(SYMCAT SECOND "-y")))

(defun smt-no-overlap (first second)
  "Return the smt-lib forms that prevent the item named FIRST from overlapping with the item named SECOND"
  #!`((assert (or
               ,(SMT-LEFT FIRST SECOND) ,(SMT-BELOW FIRST SECOND)
               ,(SMT-LEFT SECOND FIRST) ,(SMT-BELOW SECOND FIRST)))))

(defmethod smt-pair-rules append ((first item) (second item))
  "Return the smt-lib forms that will prevent the two items from overlapping"
   (smt-no-overlap (name first) (name second)))

(defun all-differing-pairs (l)
  "Return all the 2-combinations of l without repetition.
Copied from https://stackoverflow.com/a/70101069"
  (flet ((make-couples (x l) (loop for y in l collect (list x y))))
    (loop for (x . y) on l nconc (make-couples x y))))

(defun smt-rules (items)
  "Return the smt-lib forms that compute a possible layout for the items given
in ITEMS"
  (append
   (loop for item in items
         append (smt-single-rules item))
   (loop for (first second) in (all-differing-pairs items)
         append (smt-pair-rules first second))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Run them through the actual solver ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defparameter *page-width* 30)  ;; A4 landscape, in cm
(defparameter *page-height* 21)

(defparameter *smt-boilerplate*
  #!'(;; Needed to get the results later
      (set-option :produce-models true)
      ;; Integer variables, no \exists or \forall
      (set-logic QF_LIA)
      (push 1)))

(defun smt-start ()
  "Launch the SMT process and set the logic"
  (let ((answer (make-smt "z3" "-in" "-smt2")))
    (write-to-smt answer *smt-boilerplate*)
    answer))

(defparameter *smt* (smt-start)
  "The smt process")

(defun smt-new-slide (items)
  "(Re-)Initialize the smt solver to be ready to pack a new slide"
  (write-to-smt
   *smt*
   #!`((pop 1)
       (push 1)
       ;; A4 page, landscape by default, but can be changed on a per-slide basis
       (declare-fun page-width () Int)
       (assert (= page-width ,*PAGE-WIDTH*))
       (declare-fun page-height () Int)
       (assert (= page-height ,*PAGE-HEIGHT*))))
  ;; Rules for this slide:
  (write-to-smt *smt* (smt-rules items)))

(defun smt-get-values (items)
  "Return the smt-lib forms that will get us the values of the positioning variables"
  #!`((get-value ,(LOOP FOR NAME IN (LOOP FOR I IN ITEMS COLLECT (NAME I))
                        APPEND (SMT-POSITION-VARIABLES NAME)))))

(defparameter *nb-calls* 0
  "The number of calls to the smt solver")

(defun smt-size (i)
  "Return the smt-lib forms that hard-code the proposed width and height of item i"
  #!`((assert (= ,(SYMCAT (NAME I) "-width") ,(ROUND (WIDTH I))))
      (assert (= ,(SYMCAT (NAME I) "-height") ,(ROUND (HEIGHT I))))))

(defun smt-position (items)
  "Return the positions of the items so that they all fit on the page, using the
smt solver. This assumes that the smt solver has been launched and initialized
for this slide."
  (incf *nb-calls*)
  (format t "NB CALLS ~A~%" *nb-calls*)
  ;;(setf *smt-debug* t)
  (write-to-smt *smt* #!'((push 1)))
  (unwind-protect
       (progn
         (write-to-smt *smt* (loop for i in items append (smt-size i)))
         (write-to-smt *smt* #!'((check-sat)))
         (if (eql (read *smt*) 'sat)
             (write-to-smt *smt* (smt-get-values items))
             (error "Can't pack the given items"))
         (read *smt*))
    (write-to-smt *smt* #!'((pop 1)))))

(defun smt-set-positions (items)
  "Set the positions of the items on the slide"
  (let ((position (smt-position items)))  ; An alist
    ;;(format t "~A~%" position)
    (loop for i in items
          do (loop for coord in '(x y)
                   do (setf (slot-value i coord)
                            (cadr (assoc (format nil "~A-~A" (name i) coord) position
                                         :test #'string-equal)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; A normalization layer to keep the search domain within the unit cube ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun coverage-ratio (items)
  "Return the ratio, between 0 and 1, of the slide's surface that is covered by
items"
  (/ (loop for i in items sum (* (width i) (height i)) into surface finally (return surface))
     (* *page-width* *page-height*)))

(defmethod size-dimension ((i multiline))
  "Return the number of degrees of freedom of the size of a multiline piece of
  text"
  2)

(defmethod size-dimension ((i single-line))
  "Return the number of degrees of freedom of the size of a single-line piece of
  text"
  1)

(defmethod size-dimension ((i image))
  "Return the number of degrees of freedom of the size of an image"
  1)

(defun dimensionality (items)
  "Return the dimensionality of the problem of packing ITEMS on a slide,
maximizing the coverage ratio."
  (loop for i in items sum (size-dimension i) into d finally (return d)))

;; See https://graphicdesign.stackexchange.com/a/114955
;; The values here will depend on your choice of font.
;; To tweak them, look at the size of the placeholders
;; (you can do this by removing the methods for all text bearing items)
;; at the size of your items with the text.
;; If the vertical size increase, increase *x-height* so that
;; the solver knows to account for the vertical usage of your font
(defparameter *x-width* .5
  "The width of an 'x' in the smallest font-size")
(defparameter *x-height* 1.5
  "The height of an 'x' in the smallest font-size")

(defun linear-unit-interpolation (value-at-0 value-at-1 alpha)
  "Return, for alpha in [0:1], the linear interpolation of the value at alpha of
function whose values at 0 and 1 are given."
  (+ value-at-0 (* (- value-at-1 value-at-0) alpha)))

(defmethod set-size :before (i &rest scale)
  "Check that the given scale has the correct dimensionality.
FIXME: Offload this to the type system."
  (assert (= (length scale) (size-dimension i))))

(defun set-width-height (i width height)
  "Set the width and height slots of i to the given values"
  (setf (slot-value i 'width) width)
  (setf (slot-value i 'height) height))

(defmethod set-size ((i single-line) &rest scale)
  "Set the size of the single-line item i, according to SCALE"
  (let* ((alpha (elt scale 0))
        (min-width (* *x-width* (length (text i))))
        (min-height *x-height*) ; One line only
        (max-width *page-width*)
        (max-height (* min-height (/ max-width min-width)))) ; Keep ratio
    (when (> min-width *page-width*)
      (error (format nil "Too much text for item ~A to fit in a slide. Try
      writing more concisely, using a multiline item, breaking up your slide,
      increasing *page-width* and *page-height*, or decreasing *x-width* and
      *x-height*." (name i))))
    (set-width-height i
                      (linear-unit-interpolation min-width max-width alpha)
                      (linear-unit-interpolation min-height max-height alpha))))

(defmethod set-size ((i multiline) &rest scale)
  "Set the size of the multiline item i, according to SCALE"
  (let* (;; Text blocks look awful on a slide if they are taller than wide.
         ;; We limit the number of lines to a maximum value
         ;; for which the block will stay wider than tall.
         ;; In mathematical terms, we must have
         ;; nb lines * x-height <= nb chars/nb lines * x-width
         ;; <=> nb lines^2 <= nb chars * x-width / x-height
         ;; <=> nb lines <= sqrt(nb chars * x-width/x-height)
         (max-lines (max 1
                         (floor (sqrt (* (length (text i)) (/ *x-width* *x-height*))))))
         (min-width (* *x-width* (/ (length (text i)) max-lines)))
         (max-width *page-width*)
         (alpha-width (elt scale 0))
         (width (linear-unit-interpolation min-width max-width alpha-width))
         (alpha-height (elt scale 1))
         (nb-lines (linear-unit-interpolation 1 max-lines alpha-height))
         ;; The height of the item is given by
         ;; height of a line * nb lines
         ;; width of a char/x-width * x-height * nb lines
         ;; width of a char * x-height/x-width * nb lines
         ;; width of a line/nb chars per line * x-height/x-width * nb lines
         ;; width/nb chars per line * x-height/x-width * nb lines
         ;; width/(nb chars/nb lines) * x-height/x-width * nb lines
         ;; width/nb chars * x-height/x-width * nb lines^2
         (ideal-height (* (/ width (length (text i))) (/ *x-height* *x-width*) (* nb-lines nb-lines)))
         (height (max 1 (min *page-height* ideal-height))))
    (set-width-height i width height)))

(defmethod set-size ((i image) &rest scale)
  "Set the size of the image item i, according to SCALE"
  (let* ((pixel-width (pixel-width i))
         (pixel-height (pixel-height i))
         (min-ratio (/ 1 (min pixel-width pixel-height))) ;; If we resize by this ratio, the smallest dimension is now 1
         (max-ratio (min (/ *page-width* pixel-width) (/ *page-height* pixel-height))) ;; If we resize by this ratio, the biggest dimension now fits the page
         (alpha (max (slot-value i 'min-size) (elt scale 0))))
    (set-width-height i
                      (linear-unit-interpolation (* pixel-width min-ratio) (* pixel-width max-ratio) alpha)
                      (linear-unit-interpolation (* pixel-height min-ratio) (* pixel-height max-ratio) alpha))))

;;;;;;;;;;;;;;;;;;
;; Optimization ;;
;;;;;;;;;;;;;;;;;;
(defparameter *best-coverage-complement* nil
  "This is the optimum value found so far for the current slide")
(defparameter *best-point* nil
  "This is the best point found so far for the current slide")
(defparameter *max-calls* 1000
  "After this many calls to the smt solver, stop calling the smt solver")

(defmethod packing-objective (items)
  "Return the objective function that cl-grnm will minimize. Takes an array of double
as an argument"
  #'(lambda (scales)
      "Return the coverage ratio of the closed over items, given the SCALES"
      (block objective
        (assert (= (length scales) (dimensionality items)))
        ;; Check for bounds
        ;; Otherwise this little smartass will find that a negative scale factor
        ;; is a great way to optimize slide packing.
        (when (or (some #'minusp scales)
                  (some (lambda (x) (> x 1)) scales))
          (return-from objective MOST-POSITIVE-DOUBLE-FLOAT))
        ;(format t "Scales ~A~%" scales)
        (let ((scales-list (coerce scales 'list)))
          (loop for i in items
                with scale = '()
                do (setf scale (loop repeat (size-dimension i) collect (pop scales-list)))
                do (apply #'set-size i scale))
          (handler-case (progn
                          (smt-set-positions items)
                          ;(format t "Free space on slide: ~A~%"
                          ;        (coerce (- 1 (coverage-ratio items)) 'double-float))
                          (coerce (- 1 (coverage-ratio items)) 'double-float))
            (t (_)
              ;(declare (ignore _))
              (format t "Error ~A~%" _)
              MOST-POSITIVE-DOUBLE-FLOAT))))))

(defun single-pack (items scales)
  "Packs ITEMS into a slide, with the scales list SCALES, only once. Return
whether the packing was successful."
  (format t "Single packing point ~A~%" scales)
  (<
   (funcall (packing-objective items)
            (coerce scales 'vector))
   MOST-POSITIVE-DOUBLE-FLOAT))

(defun random-packable-point (items)
  "Return a random point that packs for the given slide"
  (loop with candidate = nil
        do (setf candidate (loop repeat (dimensionality items) collect (random 1.0d0)))
        when (single-pack items candidate)
          do (return candidate)))

(defun display-items (items)
  "FIXME: defin a printing method"
  (loop for i in items
        do (format t "~A~%  x ~A~%  y ~A~%  width ~A~%  height ~A~%"
                   (name i)
                   (slot-value i 'x)
                   (slot-value i 'y)
                   (slot-value i 'width)
                   (slot-value i 'height)))
  (format t "Coverage: ~A~%" (coverage-ratio items)))

(defparameter *initial-simplex-size* 3.
  "This was determined in our experiments (See exp_0001 to exp_0004) to be a very good compromise value.")
(defparameter *convergence-p* (cl-grnm:pp-volume-test 0.03d0)
   "This was determined in our experiments (See exp_0001 to exp_0004) to be a very good compromise value.")
(defun pack-slide (items)
  "Set the optimal size and position for the given items"
  (setf *best-point* nil)  ; Reset
  (setf *best-coverage-complement* nil)  ; Reset
  (setf *nb-calls* 0)  ; Reset
  (smt-new-slide items)
  (loop while (< *nb-calls* *max-calls*)
        with starting-point = nil
        do (setf starting-point (random-packable-point items))
        do (format t "Starting optimization from point ~A~%" starting-point)
        do (multiple-value-bind (candidate-point candidate-obj)
               (cl-grnm:grnm-optimize (packing-objective items)
                                      (cl-grnm:initial-simplex (coerce starting-point 'vector) :displace *initial-simplex-size*)
                                      :verbose nil
                                      :convergence-p *convergence-p*
                                      :max-function-calls (round (/ *max-calls* 3)))
             (format t "Converged on ~A with a score of ~A~%" candidate-point candidate-obj)
             (when (or (null *best-coverage-complement*)
                       (< candidate-obj *best-coverage-complement*))
               (setf *best-coverage-complement* candidate-obj)
               (setf *best-point* candidate-point)
               (format t "Setting best point to ~A~%" candidate-point))))
  ;; Set the values in items using the best knwon solution
  ;; Setting the slots of items is a side effect of the objective function
  (funcall (packing-objective items) *best-point*)
  (display-items items))

;;;;;;;;;;;;
;; Export ;;
;;;;;;;;;;;;
;; FIXME: Export to more than PDF (e.g. HTML)
(defparameter *this-directory*
  (if (null *load-truename*)
      #P"./"  ; Assume that in interactive use, the CWD is the dir where the script is
      (make-pathname :directory (pathname-directory *load-truename*)))
  "The directory in which this lisp file resides")
(djula:add-template-directory (format nil "~A/templates/" *this-directory*))
(defparameter +A4_landscape+ (djula:compile-template* "A4_landscape.tikz"))
(defparameter +node+ (djula:compile-template* "node.tikz"))
(defparameter *nb-slides* 0
  "The current slide number")

(djula:render-template* +A4_landscape+ *standard-output*)

(defparameter *tikz-colors*
'(
  "Apricot"
  "Aquamarine"
  "Bittersweet"
  "Black"
  "Blue"
  "BlueGreen"
  "BlueViolet"
  "BrickRed"
  "Brown"
  "BurntOrange"
  "CadetBlue"
  "CarnationPink"
  "Cerulean"
  "CornflowerBlue"
  "Cyan"
  "Dandelion"
  "DarkOrchid"
  "Emerald"
  "ForestGreen"
  "Fuchsia"
  "Goldenrod"
  "Gray"
  "Green"
  "GreenYellow"
  "JungleGreen"
  "Lavender"
  "LimeGreen"
  "Magenta"
  "Mahogany"
  "Maroon"
  "Melon"
  "MidnightBlue"
  "Mulberry"
  "NavyBlue"
  "OliveGreen"
  "Orange"
  "OrangeRed"
  "Orchid"
  "Peach"
  "Periwinkle"
  "PineGreen"
  "Plum"
  "ProcessBlue"
  "Purple"
  "RawSienna"
  "Red"
  "RedOrange"
  "RedViolet"
  "Rhodamine"
  "RoyalBlue"
  "RoyalPurple"
  "RubineRed"
  "Salmon"
  "SeaGreen"
  "Sepia"
  "SkyBlue"
  "SpringGreen"
  "Tan"
  "TealBlue"
  "Thistle"
  "Turquoise"
  "Violet"
  "VioletRed"
  "White"
  "WildStrawberry"
  "Yellow"
  "YellowGreen"
  "YellowOrange")
  "The colors in the dvipsname LaTeX package")

(defgeneric tikz-template-args (i)
  (:documentation "Return the args to be given to the tikz node template for the given ITEM.")
  (:method-combination append))

(defun tikz (i)
  "Return the LaTeX code for a tikz node representing item I."
  (with-output-to-string (s)
    (apply #'djula:render-template*
           +node+ s
           (tikz-template-args i))))

(defparameter *placeholder* nil
  "Activate the display of item placeholder is set to t")

(defun smallest-alpha (i)
  "SMT computed the width and height of item I by rounding its actual width and
  height. This discrepancy between the two needs to be addressed otherwise
  the exported items may be drawn with as much as half a length unit missing or
  in excess, in both dimensions.

  To avoid this we resize the width and height so that the biggest actual
  dimension is set to the SMT value, and the smallest is smaller (i.e. no overflow
  outside the SMT size."
  (let* ((true-width (slot-value i 'width))
         (smt-width (round true-width))
         (true-height (slot-value i 'height))
         (smt-height (round true-height)))
    (min (/ smt-height true-height)
         (/ smt-width true-width))))

(defun best-fit-height (i)
  "Return the height that will fit in the SMT-height"
  (format nil "~$" (* (slot-value i 'height) (smallest-alpha i))))

(defun best-fit-width (i)
  "Return the width that will fit in the SMT-width"
  (format nil "~$" (* (slot-value i 'width) (smallest-alpha i))))

(defmethod tikz-template-args append ((i item))
  "The most basic arguments to the tikz template: size, color, name, and position"
     (list
       :color (elt
               *tikz-colors* (mod (parse-integer
                                   (car (last (uiop:split-string
                                               (slot-value i 'name)
                                               :separator "-"))))
                                  (length *tikz-colors*)))
      :smt-width (round (slot-value i 'width))
      :smt-height (round (slot-value i 'height))
      :true-width (best-fit-width i)
      :true-height (best-fit-height i)
      :name (slot-value i 'name)
      :x (slot-value i 'x)
      :y (slot-value i 'y)
      :placeholder (if *placeholder* (slot-value i 'name) nil)
       ))

 (defmethod tikz-template-args append ((i image))
   "Return the image path"
      (list :image-path (slot-value i 'path)))

(defmethod tikz-template-args append ((i single-line))
  "Return the single line text"
     (list :single-line-text (slot-value i 'text)))

(defmethod tikz-template-args append ((i url))
  "Return the url"
  (list :url t))

(defmethod tikz-template-args append ((i multiline))
  "Return the multiline text"
     (list :multiline-text (slot-value i 'text)
           :text-width (floor (slot-value i 'width))))

(defmethod tikz-template-args append ((i title))
  "Set the text in small caps"
     (list :smallcaps t))

(defun slide (&rest items)
  "Compile a slide to a PDF file, return the path of said PDF file"
  (pack-slide items)  ;; Side-effect: set the items' position and size
  (let ((slide-name (format nil "slide-~3,'0d" *nb-slides*)))
    (incf *nb-slides*)
    (with-open-file (s (format nil "~A.tex" slide-name)
                       :direction :output :if-exists :supersede)
      (djula:render-template*  +A4_landscape+ s
                               :nodes (map 'list #'tikz items)))
    (uiop:run-program (format nil "latexmk -pdf ~A" slide-name))
    (format nil "~A.pdf" slide-name)))

(defun slideshow (&rest slides)
  (uiop:run-program (format nil "pdfunite ~{~A ~} slideshow.pdf" slides)))

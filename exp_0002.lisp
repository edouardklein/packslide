;; The goal of this experiment is to see whether we can get rid of the
;; hyperparameters that limit the maximum number of iterations.
;; We do that by only selecting seed points for which the packing is possible.
;; We will also try different convergence criterion.
;; We remove the choice of the next starting point for now, we'll just run through all of them.
;; Later on we'll see if by ordering the starting points we can reduce the number of calls
;; with minimal impact on performace
;; We will also record the number of iterations for each call to the search function,
;; in order to see what convergence criterion converges the fastest.


(asdf:load-system :cl-smt-lib)
(asdf:load-system :opticl)
(asdf:load-system :cl-grnm)
(in-package :cl-smt-lib)
(in-readtable :cl-smt-lib)

(load "packslide.lisp")

(defparameter *items* (list (title "Coucou") (image "pack.png")))

(defparameter *N* 10)  ;; Nb of runs per set of values

(setf *max-iterations* 300)  ;; High enough to hopefully never be reached

;; Here we redefine most of the search system, we'll copy it back later
;; in packslide.lisp once we understand what the best method is

(defun single-pack (items scales)
  "Packs ITEMS into a slide, with the scales list SCALES, only once. Return
whether the packing was successful."
  (<
   (funcall (packing-objective items)
            (coerce scales 'vector))
   MOST-POSITIVE-DOUBLE-FLOAT))

(defparameter *convergence* `(
                           :tolerance=0.05  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.05d0 :tol-x 0.05d0)
                           :tolerance=0.1  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.1d0 :tol-x 0.1d0)
                           :tolerance=0.2  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.2d0 :tol-x 0.2d0)
                           :volume=0.2 ,(cl-grnm:pp-volume-test 0.2d0)
                           :volume=0.1 ,(cl-grnm:pp-volume-test 0.1d0)
                           :volume=0.05 ,(cl-grnm:pp-volume-test 0.05d0)))


(defun pack-slide-from-2 (items start convergence-p)
  "Run the optimization algorithm with START as a starting point, reset the
best-* global variables if the value found by this run is better than what we
already knew."
  (let ((old-nb-calls *debug-nb-calls*))
    (multiple-value-bind (candidate-point candidate-obj)
        (cl-grnm:grnm-optimize (packing-objective items)
                               (cl-grnm:initial-simplex (coerce start 'vector) :displace 0.1d0)
                               :verbose nil
                               :convergence-p (getf *convergence* convergence-p)
                               :max-function-calls *max-iterations*)
      (when (or (null *best-coverage-complement*)
                (< candidate-obj *best-coverage-complement*))
        (setf *best-coverage-complement* candidate-obj)
        (setf *best-point* candidate-point)
        (format t "Setting best point to ~A~%" candidate-point))
      (with-open-file (s #P"exp_0002_call-cost.csv" :direction :output :if-exists :append :if-does-not-exist :create)
        (format s "~A,\"~A\",~A,~A~%"
                  candidate-point convergence-p candidate-obj (- *debug-nb-calls* old-nb-calls))))))

(defun pack-slide-2 (items convergence-p)
  "Set the optimal size and position for the given items"
  (setf *best-point* nil)  ; Reset
  (setf *best-coverage-complement* nil)  ; Reset
  (smt-new-slide items)
  ;; Check that the slide is packable
  (unless (single-pack items (loop repeat (dimensionality items) collect 0.0d0))
    (error "Even at minimum size, the slide is unpackable. Try increasing *page-width* and *page-height*."))
  ;; Generate the required number of packable seed points.
  ;; If the slide is hard to pack, this may take a long time.

  (let ((seed-points  ; The possible starting points of the optimization
          (loop while (< (length answer) *max-tries*)
                with answer = nil
                with candidate = nil
                do (setf candidate (loop repeat (dimensionality items) collect (random 1.0d0)))
                when (single-pack items candidate)
                     do (push candidate answer)
                finally (return answer))))
    (format t "Seed points: ~A~%" seed-points)
    ;; Log how many tries it took to get max-tries packable seed-points
    (with-open-file (s #P"exp_0002_seed_cost.csv" :direction :output :if-exists :append :if-does-not-exist :create)
      (format s "~A,~A~%"
              *max-tries* *debug-nb-calls*))
    (loop for starting-point in seed-points
          do (format t "Starting optimization from point ~A~%" starting-point)
          do (pack-slide-from-2 items starting-point convergence-p)
          do (format t "Best known point is now ~A, value: ~A~%" *best-point* *best-coverage-complement*))
    ;; Set the values in items using the best knwon solution
    ;; Setting the slots of items is a side effect of the objective function
    (funcall (packing-objective items) *best-point*)
    (display-items items)))


(defun run-exp (mt convergence-p)
  (setf *max-tries* mt)
  (setf *max-tries-with-no-improvement* mt)  ;; Set and record mtni as mt, in order to easly compare with exp1.  We will study its impact later on.
  (loop repeat *N*
        do (setf *debug-nb-calls* 0)
        do (pack-slide-2 *items* convergence-p)
        do (with-open-file (s #P"exp_0002.csv" :direction :output :if-exists :append :if-does-not-exist :create)
             (format s "~A,~A,~A,~A,~A,~A~%"
                     *max-tries* *max-tries-with-no-improvement* *max-iterations* *debug-nb-calls* *best-coverage-complement* convergence-p))))

(loop for mt in '(30 100 300)
      do (loop for cp in '(:volume=0.2 :tolerance=0.2
                       ;:volume=0.1 :tolerance=0.1
                       :volume=0.05 :tolerance=0.05)
            do (run-exp mt cp)))

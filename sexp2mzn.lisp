(load "/home/edouard/.sbclrc")
;(read (open (nth 1 *posix-argv*)))

(ql:quickload :opticl)

(defun mzn-text-data (prefix text)
  "Return the mzn code for the data part of the optimization problem"
  (format nil "int: ~A_size = ~A;
"
           prefix
           (length text)))

(defun mzn-image-data (prefix path)
  "Return the mzn code for the data part of the optimization problem"
  (destructuring-bind (height width _)
      (array-dimensions (opticl:read-png-file path))
    (declare (ignore _))
    (format nil "float: ~A_ratio = ~A/~A;
"
             prefix width height)))

(defun mzn-text-variables (prefix)
  "Return the mzn code for the optimization variable for the positioning of a piece of text"
  (format nil "
var 0..width-1: ~A_x;
var 0..height-1: ~A_y;
var 1..width: ~A_width;
var 1..width: ~A_height;
var 1..10: ~A_scale;
" prefix prefix prefix prefix prefix))

(defun mzn-image-variables (prefix)
  "Return the mzn code for the optimization variables for the positionning of an image"
  (format nil "
var 0..width-1: ~A_x;
var 0..height-1: ~A_y;
var 1..width: ~A_width;
var 1..height: ~A_height;
" prefix prefix prefix prefix))

(defun mzn-text-constraints (prefix)
  "Return the mzn code that constrains a piece of text"
  (format nil "
% Big enough to hold the text
constraint ~A_width * ~A_height > ~A_size * ~A_scale * ~A_scale;
% Ratio is horizontal
constraint ~A_width > ~A_height;
% Placed inside the slide
constraint ~A_x + ~A_width < width;
constraint ~A_y + ~A_height < height;
" prefix prefix prefix prefix prefix prefix prefix
prefix prefix prefix prefix ))

(defun mzn-image-constraints (prefix)
  "Return the mzn code that constrains an image"
  (format nil "
%% Image ratios are kept (whitin the limits that the widht and height are integers)
constraint (~A_width + 1) / ~A_height > ~A_ratio;
constraint (~A_width - 1) / ~A_height < ~A_ratio;
constraint ~A_width / (~A_height - 1) > ~A_ratio;
constraint ~A_width / (~A_height + 1) < ~A_ratio;
% Placed inside the slide
constraint ~A_x + ~A_width < width;
constraint ~A_y + ~A_height < height;
" prefix prefix prefix prefix prefix prefix prefix prefix prefix prefix prefix
prefix prefix prefix prefix prefix))

(defun mzn-relative-constraints (prefix &rest others)
  "Return the mzn constraint make sure the given element
does not overlap the others"
  (apply #'concatenate 'string
         (loop for o in others
               collect (format nil "constraint (
% one above the other
(~A_y + ~A_height < ~A_y) \\/
% one below the other
(~A_y + ~A_height < ~A_y) \\/
% one left of the other
(~A_x + ~A_width < ~A_x) \\/
% One right of the other
(~A_x + ~A_width < ~A_x)
);
"
                               prefix prefix o
                               o o prefix
                               prefix prefix o
                               o o prefix))))

(defun mzn-text-objective (prefix)
  "Return the size of the text in the allocated box"
  (format nil "~A_size*~A_scale*~A_scale"
          prefix prefix prefix))

(defun mzn-image-objective (prefix)
  "Return the size of the image"
  (format nil "~A_width*~A_height"
          prefix prefix))

(defun text (type text nb &rest others)
  "Return a list:
- the prefix
- the mzn problem data
- the mzn optimization variables
- the mzn constraints that only concern this element
- the mzn constraints for this elements in relation to the other elements.
- the objective function to maximize
    "
  (let ((prefix (format nil "~A_~A" type nb)))
    `(,prefix
      ,(mzn-text-data prefix text)
      ,(mzn-text-variables prefix)
      ,(mzn-text-constraints prefix)
      ,(apply #'mzn-relative-constraints prefix others)
      ,(mzn-text-objective prefix))))

(defun title (text nb &rest others)
  "Return the mzn list for the title"
  (apply #'text "title" text nb others))

(defun bullet (text nb &rest others)
  "Return the mzn list for a bullet point"
  (apply #'text "bullet" text nb others))

(defun image (path nb &rest others)
  (let ((prefix (format nil "image_~A" nb)))
    `(,prefix
      ,(mzn-image-data prefix path)
      ,(mzn-image-variables prefix)
      ,(mzn-image-constraints prefix)
      ,(apply #'mzn-relative-constraints prefix others)
      ,(mzn-image-objective prefix)
      )))

(defun prefixes (l)
  "Collect the prefixes (the first element) from the list
of expanded elements l"
  (loop for e in l
        collect (nth 0 e)))

(defmacro slide (&body elements)
  "Call each element with the context it needs"
  `(let ((mzn-elements
           (loop for e in (quote ,elements)
                 with n = 0
                 with sofar = '()
                 do (setf sofar
                          (cons
                           (apply #'funcall
                                  (append e
                                          `(,n ,@(prefixes sofar))))
                           sofar))
                 do (incf n)
                 finally (return sofar))))
     (output-mzn mzn-elements)))

(defun output-mzn (mzn-elements)
  "Output the mzn code in the given elements"
  (format t "% Problem settings
int: width = 30;
int: height = 21;

% Problem data
")
  (loop for e in mzn-elements
        do (format t "~A" (nth 1 e)))
  (format t "% Problem variables
")
  (loop for e in mzn-elements
        do (format t "~A" (nth 2 e)))
  (format t "%Individual constraints
")
  (loop for e in mzn-elements
        do (format t "~A" (nth 3 e)))
  (format t "%Relative constraints
")
  (loop for e in mzn-elements
        do (format t "~A" (nth 4 e)))
  (format t "% Objective function
")
  (format t "solve maximize ~{~A~^+ ~};
"
          (loop for e in mzn-elements
                collect (nth 5 e)))
  )

(eval (read (open "slide1.lisp")))

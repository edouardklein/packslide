;; The goal of this experiment is to make sure that the methode we've found to
;; be delivering the best performance/cost ratio in the previous experiment also
;; deliver good performance when given more complex slides
(asdf:load-system :cl-smt-lib)
(asdf:load-system :opticl)
(asdf:load-system :cl-grnm)
(in-package :cl-smt-lib)
(in-readtable :cl-smt-lib)
(load "packslide.lisp")
(defparameter *slides* `(
                         (:2D-1 ,(list (title "Coucou") (image "pack.png")))
                         (:2D-2 ,(list (image "pack.png") (image "pack.png")))
                         (:3D-1 ,(list (title "A very long title that could be broken up" :multiline t) (image "pack.png")))
                         (:3D-2 ,(list (image "pack.png") (image "pack.png") (image "pack.png")))
                         (:4D-1 ,(list (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png")))
                         (:4D-2 ,(list (title "Coucou") (image "pack.png") (image "pack.png") (image "pack.png")))
                         (:5D-1 ,(list (title "Coucou") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png")))
                         (:6D-1 ,(list (title "Coucou") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png")))
                         (:7D-1 ,(list (title "Coucou") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png")))
                         (:8D-1 ,(list (title "Coucou") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png") (image "pack.png")))))


(setf *max-iterations* 300)  ;; High enough to hopefully never be reached
(defparameter *N* 100)  ;; Nb of runs per set of values

(setf *random-state* (make-random-state t))

(defparameter *convergence* `(
                           :tolerance=0.003  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.003d0 :tol-x 0.003d0)
                           :tolerance=0.01  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.01d0 :tol-x 0.01d0)
                           :tolerance=0.03  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.03d0 :tol-x 0.03d0)
                           :tolerance=0.1  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.1d0 :tol-x 0.1d0)
                           :tolerance=0.3  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.3d0 :tol-x 0.3d0)
                           :volume=0.003 ,(cl-grnm:pp-volume-test 0.003d0)
                           :volume=0.01 ,(cl-grnm:pp-volume-test 0.01d0)
                           :volume=0.03 ,(cl-grnm:pp-volume-test 0.03d0)
                           :volume=0.1 ,(cl-grnm:pp-volume-test 0.1d0)
                           :volume=0.3 ,(cl-grnm:pp-volume-test 0.3d0)))

(defun single-pack (items scales)
  "Packs ITEMS into a slide, with the scales list SCALES, only once. Return
whether the packing was successful."
  (format t "Single packing point ~A~%" scales)
  (<
   (funcall (packing-objective items)
            (coerce scales 'vector))
   MOST-POSITIVE-DOUBLE-FLOAT))

(defun random-packable-point (name items)
  "Return a random point that packs for the given slide"
  (loop with candidate = nil
        with nb-calls = 0
        do (incf nb-calls)
        do (setf candidate (loop repeat (dimensionality items) collect (random 1.0d0)))
        when (single-pack items candidate)
          do (progn
               (with-open-file (s #P"exp_0004_seed_cost.csv" :direction :output :if-exists :append :if-does-not-exist :create)
                 (format s "~A,~A,~A~%"
                         name (dimensionality items) nb-calls))
               (format t "Returngin packable point ~A~%" candidate)
               (return candidate))))

(defun run-exp (name items start method parameter size)
  "Run the search function once."
  (setf *debug-nb-calls* 0)
  (let ((convergence-sym (intern (format nil "~A=~A" (string-upcase method) parameter) "KEYWORD"))
        (start-time nil)
        (end-time nil))
    (format t "Starting optimization from point ~A with method ~A (~A)~%" start convergence-sym (getf *convergence* convergence-sym))
    (setf start-time (get-internal-run-time))
    (multiple-value-bind (candidate-point candidate-obj)
        (cl-grnm:grnm-optimize (packing-objective items)
                               (cl-grnm:initial-simplex (coerce start 'vector) :displace size)
                               :verbose nil
                               :convergence-p (getf *convergence* convergence-sym)
                               :max-function-calls *max-iterations*)
      (setf end-time (get-internal-run-time))
      (format t "Converged on ~A with a score of ~A~%" candidate-point candidate-obj)
      (when (< candidate-obj 0)
          (break))
      (with-open-file (s #P"exp_0004.csv" :direction :output :if-exists :append :if-does-not-exist :create)
        (format s "~A,~A,\"~A\",\"~A\",~A,~A,~A,~A,~A~%"
                name (dimensionality items) start candidate-point convergence-sym size candidate-obj *debug-nb-calls*
                (/ (- end-time start-time) internal-time-units-per-second))))))

(loop repeat *N*
      do (loop for (slide-name slide) in *slides*
               with starting-point = nil
               do (format t "*slides*  is ~A, Slide name is ~A, slide is ~A~%" *slides* slide-name slide)
               do (smt-new-slide slide)
               do (setf starting-point (random-packable-point slide-name slide))
               do (loop for convergence-method in '("volume")
                        do (loop for convergence-hyperparameter in '(0.003 0.01 0.03)
                                 do (loop for initial-simplex-size in '(1.0d0 3.0d0 10.0d0)
                                          do (run-exp slide-name slide
                                                      starting-point
                                                      convergence-method
                                                      convergence-hyperparameter
                                                      initial-simplex-size))))))

;; Debug code for interactive use
;; (setf *slide* (cadr (assoc :8D-1 *slides*)))
;; (setf *optimization-result* #(
;;                            0.35369646785833714d0 0.31452183745422235d0
;;                0.44836450552210444d0 0.1203282497355148d0 0.4599969235954813d0
;;                0.2066293300312418d0 0.48861843285094175d0 0.533479550587163d0
;;                               ))
;; (setf *optimization-start* #(
;;                              0.35197289507919494d0 0.12112813279970158d0 0.5203126205078357d0 0.05363924143485854d0 0.40917362369769816d0 0.09565775955877798d0 0.46461843285094173d0 0.2558517506237947d0
;;                               ))
;; ;; Reset the smt
;; (setf *smt* (smt-start))
;; (smt-new-slide *slide*)
;; ;; Check that the initial value is packing
;; (funcall (packing-objective *slide*) (coerce  *optimization-start* 'vector))
;; ;; Check that the result is packing
;; (funcall (packing-objective *slide*) (coerce  *optimization-result* 'vector))
;; ;; Rerun the optimization from (supposedly) the same conditions
;; (cl-grnm:grnm-optimize (packing-objective *slide*)
;;                                (cl-grnm:initial-simplex (coerce *optimization-start* 'vector) :displace 1.0)
;;                                :verbose nil
;;                                :convergence-p (getf *convergence* :VOLUME=0.03)
;;                                :max-function-calls *max-iterations*)

(use-modules (guix packages))
(use-modules (guix utils))
(use-modules (guix gexp))
(use-modules (gnu packages))
(use-modules (gnu packages base))
(use-modules (gnu packages perl))
(use-modules (gnu packages bash))
(use-modules (gnu packages pdf))
(use-modules (gnu packages lisp))
(use-modules (gnu packages lisp-xyz))
(use-modules (gnu packages readline))
(use-modules (gnu packages maths))
(use-modules (gnu packages tex))
(use-modules (gnu packages pdf))
(use-modules (guix profiles))
(use-modules (guix download))
(use-modules (guix build-system asdf))
(use-modules (guix build-system copy))
(use-modules (guix licenses))
(use-modules (guix git-download))
(use-modules (gnu packages python))
(use-modules ((guix licenses) #:prefix license:))
(use-modules (ice-9 popen))
(use-modules (ice-9 textual-ports))
(use-modules (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define %git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f2" OPEN_READ)))

(define (skip-git-and-build-directory file stat)
  "Skip the `.git` and `build` and `guix_profile` directory when collecting the sources."
  (let ((name (basename file)))
    (not (or (string=? name ".git")
             (string=? name "build")
             (string-prefix? "guix_profile" name)))))

(define *version* (call-with-input-file "VERSION"
                    get-string-all))

(define-public sbcl-cl-smt-lib
  (package
   (name "sbcl-cl-smt-lib")
   (version "1.0.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/GrammaTech/cl-smt-lib.git")
           (commit "9ace6776899ff4b7b8f62bd25b75010dc60d10df")))
     (sha256
      (base32
       "1rbyz2h9lp9fzzzvc4pqh9l0fjv4ywqyffcb4b57ybb00gzhwpzn"))
     (file-name (git-file-name name version))))
   (build-system asdf-build-system/sbcl)
   (native-inputs
    (list sbcl-named-readtables sbcl-trivial-gray-streams))
   (synopsis "osef")
   (description
    "osef")
   (home-page "https://github.com/GrammaTech/cl-smt-lib")
   (license license:bsd-4)))

(define-public sbcl-cl-grnm
  (package
   (name "sbcl-cl-grnm")
   (version "0.1.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/quil-lang/cl-grnm.git")
           (commit "778a312e9eb3d3de7541457b516ea3d9055a15aa")))
     (sha256
      (base32
       "1hb5n37n3x2ylrghcqsia2g9a6f5wg24l659jiz4ncpi5bsv4m3s"))
     (file-name (git-file-name name version))))
   (build-system asdf-build-system/sbcl)
   ;(native-inputs
   ; (list sbcl-named-readtables sbcl-trivial-gray-streams))
   (synopsis "osef")
   (description
    "osef")
   (home-page "https://github.com/GrammaTech/cl-smt-lib")
   (license license:expat)))

(define-public packslide
  (package
   (name "packslide")
   (version (git-version *version* "HEAD" %git-commit))
   (source (local-file %source-dir
                       #:recursive? #t
                       #:select? skip-git-and-build-directory))
   ;; Not ideal, we are like in Python where everything is propagated.
   ;; I should learn how to properly package common lisp applications
   (propagated-inputs
    (list
     rlwrap sbcl cl-asdf sbcl-cl-smt-lib
     sbcl-slime-swank z3
     sbcl-opticl sbcl-cl-grnm sbcl-djula poppler texlive-bin texlive-latexmk
     texlive-pgf texlive-standalone texlive-shapepar texlive-cm-super texlive-collection-fontsrecommended))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan '(
                       ("./" "bin/")
                       )
      #:phases
      (modify-phases %standard-phases
                     (add-before
                      'build 'check
                      (lambda* (#:key outputs #:allow-other-keys)
                               (let* ((dir (string-append
                                            (assoc-ref outputs "out")
                                            "/bin/")))
                                 (chdir dir)
                                 ;;(invoke "make" "test") ;Not yet
                                 #t))))
      ))
   (synopsis "Later")
   (description
    "Later")
   (home-page "Later")
   (license license:agpl3+)))

packslide

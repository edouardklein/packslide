;; This time we will just start from a single random point, and from this point,
;; run all configurations of the search. In the analysis, we'll draw from these runs
;; to simulate what would have happened on a given budget.
(asdf:load-system :cl-smt-lib)
(asdf:load-system :opticl)
(asdf:load-system :cl-grnm)
(in-package :cl-smt-lib)
(in-readtable :cl-smt-lib)
(load "packslide.lisp")
(defparameter *items* (list (title "Coucou") (image "pack.png")))
(setf *max-iterations* 300)  ;; High enough to hopefully never be reached
(defparameter *N* 100)  ;; Nb of runs per set of values
(smt-new-slide *items*)

(setf *random-state* (make-random-state t))

(defparameter *convergence* `(
                           :tolerance=0.003  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.003d0 :tol-x 0.003d0)
                           :tolerance=0.01  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.01d0 :tol-x 0.01d0)
                           :tolerance=0.03  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.03d0 :tol-x 0.03d0)
                           :tolerance=0.1  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.1d0 :tol-x 0.1d0)
                           :tolerance=0.3  ,(cl-grnm:burmen-et-al-convergence-test :tol-f 0.3d0 :tol-x 0.3d0)
                           :volume=0.003 ,(cl-grnm:pp-volume-test 0.003d0)
                           :volume=0.01 ,(cl-grnm:pp-volume-test 0.01d0)
                           :volume=0.03 ,(cl-grnm:pp-volume-test 0.03d0)
                           :volume=0.1 ,(cl-grnm:pp-volume-test 0.1d0)
                           :volume=0.3 ,(cl-grnm:pp-volume-test 0.3d0)))

(defun single-pack (items scales)
  "Packs ITEMS into a slide, with the scales list SCALES, only once. Return
whether the packing was successful."
  (format t "Single packing point ~A~%" scales)
  (<
   (funcall (packing-objective items)
            (coerce scales 'vector))
   MOST-POSITIVE-DOUBLE-FLOAT))

(defun random-packable-point (items)
  "Return a random point that packs for the given slide"
  (loop with candidate = nil
        with nb-calls = 0
        do (incf nb-calls)
        do (setf candidate (loop repeat (dimensionality items) collect (random 1.0d0)))
        when (single-pack items candidate)
          do (progn
               (with-open-file (s #P"exp_0003_seed_cost.csv" :direction :output :if-exists :append :if-does-not-exist :create)
                 (format s "~A,~A~%"
                         (dimensionality items) nb-calls))
               (format t "Returngin packable point ~A~%" candidate)
               (return candidate))))

(defun timings (function)
  (let ((run-base (get-internal-run-time)))
    (funcall function)
    (/ (- (get-internal-run-time) run-base) internal-time-units-per-second)))

(defun run-exp (items start method parameter size)
  "Run the search function once."
  (setf *debug-nb-calls* 0)
  (let ((convergence-sym (intern (format nil "~A=~A" (string-upcase method) parameter) "KEYWORD"))
        (start-time nil)
        (end-time nil))
    (format t "Starting optimization from point ~A with method ~A (~A)~%" start convergence-sym (getf *convergence* convergence-sym))
    (setf start-time (get-internal-run-time))
    (multiple-value-bind (candidate-point candidate-obj)
        (cl-grnm:grnm-optimize (packing-objective items)
                               (cl-grnm:initial-simplex (coerce start 'vector) :displace size)
                               :verbose nil
                               :convergence-p (getf *convergence* convergence-sym)
                               :max-function-calls *max-iterations*)
      (setf end-time (get-internal-run-time))
      (format t "Converged on ~A with a score of ~A~%" candidate-point candidate-obj)
      (with-open-file (s #P"exp_0003.csv" :direction :output :if-exists :append :if-does-not-exist :create)
        (format s "~A,\"~A\",~A,~A,~A,~A,~A~%"
                start candidate-point convergence-sym size candidate-obj *debug-nb-calls*
                (/ (- end-time start-time) internal-time-units-per-second))))))

(loop repeat *N*
      with starting-point = nil
      do (setf starting-point (random-packable-point *items*))
      do (loop for convergence-method in '("volume" "tolerance")
               do (loop for convergence-hyperparameter in '(0.003 0.01 0.03)
                        do (loop for initial-simplex-size in '(3.0d0 10.0d0)
                                 do (run-exp *items*
                                             starting-point
                                             convergence-method
                                             convergence-hyperparameter
                                             initial-simplex-size)))))

;; The goal of this experiment is to understand how to best pack a slide while
;; minimizing the number of times the SAT solver has to run.


(asdf:load-system :cl-smt-lib)
(asdf:load-system :opticl)
(asdf:load-system :cl-grnm)
(in-package :cl-smt-lib)
(in-readtable :cl-smt-lib)

(load "packslide.lisp")

(defparameter *items* (list (title "Coucou") (image "pack.png")))

(defparameter *N* 10)  ;; Nb of runs per set of values

(defun run-exp (mt mtni mi)
  (setf *max-tries* mt)
  (setf *max-tries-with-no-improvement* mtni)
  (setf *max-iterations* mi)
  (loop repeat *N*
      do (setf *debug-nb-calls* 0)
      do (pack-slide *items*)
      do (with-open-file (s #P"exp_0001.csv" :direction :output :if-exists :append :if-does-not-exist :create)
           (format s "~A,~A,~A,~A,~A~%"
                   *max-tries* *max-tries-with-no-improvement* *max-iterations* *debug-nb-calls* *best-coverage-complement*))))
;; (loop for mt in '(10 30 100)
;;       do (loop for mtni in '(3 10 30)
;;                do (loop for mi in '(10 30 100)
;;                         do (run-exp mt mtni mi))))
;; (setf *N* 30)
;; (run-exp 30 10 30)
;; (run-exp 10 10 100)
;; (run-exp 100 10 100)


;; (setf *N* 10)
;; (loop for mt in '(30 100 300)
;;       do (loop for mtni in '(30 100 300)
;;                when (>= mt mtni)
;;                  do (loop for mi in '(10 30 100 300)
;;                           do (run-exp mt mtni mi))))

(setf *N* 10)
(run-exp 300 100 30)
(run-exp 300 100 100)
(run-exp 300 300 100)
(run-exp 300 300 300)

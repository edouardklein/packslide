#!/usr/bin/env bash
set -euxo pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"/"
ASDF_OUTPUT_TRANSLATIONS="/:$HOME/.cache/common-lisp/my-dir/" sbcl \
    --load $GUIX_ENVIRONMENT/share/common-lisp/source/asdf/asdf.lisp \
    --eval  '(setf asdf:*central-registry* '"'"'(#P"'$SCRIPT_DIR'"))' \
    --eval '(asdf:load-system :cl-smt-lib)' \
    --eval '(asdf:load-system :opticl)' \
    --eval '(asdf:load-system :cl-grnm)' \
    --eval '(in-package :cl-smt-lib)' \
    --eval '(in-readtable :cl-smt-lib)' \
    --load 'packslide.lisp' \
    --eval '(defparameter *items* (list (title "Coucou") (image "pack.png")))' \
    --eval '(pack-slide *items*)' \
    --eval '(pack-slide *items*)' \
    --eval '(pack-slide *items*)' \
    --eval '(pack-slide *items*)'

(in-package :cl-smt-lib)
(slideshow
  (slide
 	(image "pack.png")
 	(title "Automated slide layout")
	 (bullet "Focus exclusively on the content")
	 (bullet "A constraint solver handles the layout")
	 (bullet "Tweak the style later if you want to")
 	(p "This example slide contains an image, a title, three bullet points, and a
longer paragraph.")))


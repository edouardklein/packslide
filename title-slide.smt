;; Needed to get the results later
(set-option :produce-models true)
;; Integer variables, no \exists or \forall
(set-logic QF_LIA)
;; A4 page, landscape
(declare-fun page-width () Int)
(assert (= page-width 30))
(declare-fun page-height () Int)
(assert (= page-height 21))

;; Variables
;; The title position
(declare-fun title-x () Int)
(declare-fun title-y () Int)
;; The title size
(declare-fun title-width () Int)
(declare-fun title-height () Int)

;; Image position
(declare-fun image-x () Int)
(declare-fun image-y () Int)
;; Image size
(declare-fun image-width () Int)
(declare-fun image-height () Int)


;; Constraints
;; Every variable is positive
(assert (>= title-x 0))
(assert (>= title-y 0))
(assert (>= title-width 0))
(assert (>= title-height 0))
(assert (>= image-x 0))
(assert (>= image-y 0))
(assert (>= image-width 0))
(assert (>= image-height 0))
;; Input size, let's see if it fits
(assert (= title-width 28))
(assert (= title-height 8))
(assert (= image-width 30))
(assert (= image-height 13))
;; Title must fit in the page
(assert (<= (+ title-x title-width) page-width))
(assert (<= (+ title-y title-height) page-height))
;; Image must fit in the page
(assert (<= (+ image-x image-width) page-width))
(assert (<= (+ image-y image-height) page-height))
;; No overlaps
(assert (or
         (<= (+ title-x title-width) image-x)
         (<= (+ title-y title-height) image-y)
         (<= (+ image-x image-width) title-x)
         (<= (+ image-y image-height) title-y)
         ))

;; Title is left of, or above any other items
(assert (or
         (< title-x image-x)
         (> title-y image-y)))

(check-sat)
(get-value (title-x title-y title-width title-height
            image-x image-y image-width image-height))
(exit)
